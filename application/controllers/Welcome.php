<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Model_signin');
        $this->load->library('session');
    }

    public function index() {
        //print_r ($_SESSION);
        // exit;
        if (isset($_SESSION['Email']) && $_SESSION['Email'] != "") {
            $this->load->view('taskmanager');
        } else {
            $this->load->view('login');
        }
    }

    function login() {
        $this->load->view('login');
    }

    function logout() {
        $sessionData = array(
            'Firstname' => " ",
            'Lastname' => " ",
            'Email' => " ",
        );
        $this->session->set_userdata($sessionData);
        $this->session->unset_userdata($sessionData);
        $this->session->sess_destroy();
        // redirect("login");
        $this->load->view('login');
    }

    function signup_view() {
        $this->load->view('signup');
    }

    function Updatetask() {
        if (isset($_SESSION['Email']) && $_SESSION['Email'] != "") {
            $asigneedept = $this->input->post('asigneedept');
            $assigndept = $this->input->post('assigndept');
            $priority = $this->input->post('priority');
            $Task = $this->input->post('Task');
            $task_id = $this->input->post('id');
            $data = $this->Model_signin->UpdateTaskData($task_id, $asigneedept, $assigndept, $priority, $Task);
            if ($data > 0) {

                echo "correct";
            } else {
                echo 'wrong';
            }
        } else {
            //echo 'wrong';
            $this->load->view('login');
        }
    }

    function gettask() {
        if (isset($_SESSION['Email']) && $_SESSION['Email'] != "") {
            $task_id = $this->input->post('id');
            $data = $this->Model_signin->TaskData($task_id);
            if ($data > 0) {
                $output['task'] = $data;
                //$output['check'] = $this->Model_signin->getuserData();
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode($output));
            }
        } else {
            //echo 'wrong';
            $this->load->view('login');
        }
    }

    function completetask() {
        if (isset($_SESSION['Email']) && $_SESSION['Email'] != "") {
            $task_id = $this->input->post('id');
            $opt = $this->session->userdata();
            $email_id = $opt ['Email'];
            $data = $this->Model_signin->completeTaskData($task_id, $email_id);
            if ($data > 0) {
                $output['check2'] = $this->Model_signin->getuserData();
                //$output['check'] = $this->Model_signin->getuserData();
                $this->output->set_content_type('application/json');
                $this->output->set_output(json_encode($output));
            }
        } else {
            //echo 'wrong';
            $this->load->view('login');
        }
    }

    function addtask() {
        if (isset($_SESSION['Email']) && $_SESSION['Email'] != "") {
            $asigneedept = $this->input->post('asigneedept');
            $assigndept = $this->input->post('assigndept');
            $priority = $this->input->post('priority');
            $Task = $this->input->post('Task');
            $data = $this->session->userdata();
            //print_r($_POST);
            $firstname = $data ['Firstname'];
            $lastname = $data ['Lastname'];
            $email_id = $data ['Email'];
            date_default_timezone_set("Asia/Calcutta");
//      $dt= date("Y-m-d")   ;$tm=date("h:i:sa");

            $name = $firstname . " " . $lastname;
            //$date="";
            $date = date('Y-m-d H:i:s');
            // echo $name. " ". $email_id ."  ".$asigneedept ."  ".$assigndept ." ".$priority ."  " .$Task. $date;//done

            $data = $this->Model_signin->InsertTaskData($email_id, $name, $asigneedept, $assigndept, $priority, $Task, $date);
            if ($data > 0) {

                echo "correct";
            } else {
                echo 'wrong';
            }
        } else {
            //echo 'wrong';
            $this->load->view('login');
        }
    }

    function signup() {
        $firstname = $this->input->post('user_firstame');
        $lastname = $this->input->post('user_lastname');
        $email_id = $this->input->post('user_email');
        $password = $this->input->post('user_password');
        //print_r($_POST);

        $data = $this->Model_signin->InsertData($firstname, $lastname, $email_id, $password);
        if ($data > 0) {
            $sessionData = array(
                'Firstname' => $firstname,
                'Lastname' => $lastname,
                'Email' => $email_id,
            );
            $this->session->set_userdata($sessionData);
            echo "correct";
        } else {
            echo 'wrong';
        }
    }

    function signin() {
        $email_id = $this->input->post('user_email');
        $password = md5($this->input->post('user_password'));
        $query = $this->db->query("Select * from user where email='$email_id' and password= '$password'   LIMIT 1")->row();
        // echo $query->id;
        //  exit;



        if (count($query) > 0) {
            // $this->session->unset_userdata('username');
            $sessionData = array(
                'Firstname' => $query->Firstname,
                'Lastname' => $query->Lastname,
                'Email' => $query->email,
            );
            $this->session->set_userdata($sessionData);
            echo "correct";
        } else {
            echo 'wrong';
        }

        //  print_r($data);exit;
    }

    function userdata() {//echo hello;
        //exit;
        if (isset($_SESSION['Email']) && $_SESSION['Email'] != "") {
            $data['check'] = $this->Model_signin->getuserData();
            $data['check1'] = $this->session->userdata();

            // print_r($data);
            // exit;
//         print json_encode($data
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($data));
        } else {
            //echo 'wrong';
            $this->load->view('login');
        }
    }

    function completedTaskData() {//echo hello;
        //exit;
        if (isset($_SESSION['Email']) && $_SESSION['Email'] != "") {
            $data['check'] = $this->Model_signin->getCompletedData();
            $data['check1'] = $this->session->userdata();

            // print_r($data);
            // exit;
//         print json_encode($data
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($data));
        } else {
            //echo 'wrong';
            $this->load->view('login');
        }
    } 
}
