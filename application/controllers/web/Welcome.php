<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Model_signin');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index() {
        if ($this->check_logged_in()) {
            $user_id = $this->session->userdata["user_data"][0]['user_id'];
            $rows = $this->Common->getData("users", "user_id = $user_id")->result_array();
            $output['rows'] = $rows; //user_details
            $this->load->view('taskmanager', $output);
        } else {
            redirect(BASEURL . "web/Welcome/login");
        }
    }

    function login() {
        if ($this->check_logged_in()) {
            redirect(BASEURL . "web/Welcome/index");
        } else {
            $this->load->view('login');
        }
    }

    function logout() {
        session_destroy();
        $this->session->sess_destroy();
//        $this->googleplus->revokeToken();
        $this->session->unset_userdata('user_data');
        redirect(BASEURL . "web/Welcome/login");
    }

    function register() {
        if ($this->check_logged_in()) {
            redirect(BASEURL . "web/Welcome/index");
        } else {
            $this->load->view('signup');
        }
    }

    public function check_logged_in() {
        if ($this->session->userdata('user_data') != NULL) {
            return true;
        } else {
            return false;
        }
    }

}
