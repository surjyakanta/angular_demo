<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Model_signin');
        $this->load->library('session');
           $this->load->helper('url');
    }

    public function index() {
        //print_r ($_SESSION);
       // exit;
        if (isset($_SESSION['Email']) && $_SESSION['Email']!="" ) {
        $this->load->view('taskmanager');
        }
        else {
            $this->load->view('login');
        }
    }

    function login() {
        if (isset($_SESSION['Email']) && $_SESSION['Email']!="" ) {
            redirect("welcome/index");
        }
        else {
        $this->load->view('login');
        }
    }
    function logout(){
        $sessionData = array(
                'Firstname' => " ",
                'Lastname' => " ",
                'Email' => " ",
            );
            $this->session->set_userdata($sessionData);
             $this->session->unset_userdata($sessionData);
        $this->session->sess_destroy();
       // redirect("login");
        $this->load->view('login');
    }
                function signup_view() {
        $this->load->view('signup');
    }

   

}
