


<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Model_signin');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('common');
    }

    function Task_assign_status() {
        $output = array();
        if (count($_POST) > 0) {

            $user_id = $this->input->post("user_id");
            $where = " tasks.created_by_user_id = $user_id  and tasks.is_complete=0 and tasks.is_assigned>0 ";
        $join_cond = "tasks.is_assigned =users.user_id";
        $result_data = $this->Common->getJoinedData("tasks.task_id,tasks.task_description ,tasks.priority ,tasks.is_accepted,users.u_email  ", 'tasks', 'users', $join_cond, $where)->result_array();
          if (count($result_data) > 0) {
            $output["success"] = TRUE;
            $output["message"] = "A task is assign to you ";
            $output["task"] = $result_data;
        } else {

            $output["message"] = "no task assign";
        } 
            
        } else {
            $output["success"] = FALSE;
            $output["message"] = "no data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function notification_action() {
        $output = array();
        if (count($_POST) > 0) {

            $action = $this->input->post("action");
            if ($action == 1) {
                $data['accepted_by_user_id'] = $user_id = $this->input->post("user_id");
                $data['task_id '] = $task_id = $this->input->post("task_id");
                date_default_timezone_set("Asia/Calcutta");
                $data['accepted_date'] = $date = date('Y-m-d H:i:s');
                $data = $this->Common->addData("taskmanage", $data);
                $update = array(
                    "is_accepted" => 1,
                );
                $where = "task_id=$task_id";
                $data = $this->Common->update("tasks", $update, $where);
                if (count($data) > 0) {
                    $output["success"] = TRUE;
                    $output["message"] = "Task accepted";
                } else {
                    $output["success"] = FALSE;
                    $output["message"] = "some problem occoured";
                }
            } else {
                $data['task_id '] = $task_id = $this->input->post("task_id");
                $data['is_cancel  '] = 1;
                $data = $this->Common->addData("taskmanage", $data);
                $update = array(
                    "is_accepted" => 2,
                );
                $where = "task_id=$task_id";
                $data = $this->Common->update("tasks", $update, $where);
                if (count($data) > 0) {
                    $output["success"] = TRUE;
                    $output["message"] = "Task Rejected";
                } else {
                    $output["success"] = FALSE;
                    $output["message"] = "some problem occoured";
                }
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "no data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function notification_asgn_user_task() {
        $user_id = $this->input->post("user_id");
        $where = " tasks.is_assigned = $user_id and tasks.is_accepted =0 and tasks.is_complete=0 ";
        $join_cond = "tasks.created_by_user_id=users.user_id";
        $result_data = $this->Common->getJoinedData("tasks.task_id,tasks.task_description ,tasks.priority ,users.u_email  ", 'tasks', 'users', $join_cond, $where)->result_array();
        // $result_data = $this->Common->getSelectData("task_id,created_by_user_id ,priority ,task_description", "tasks", " is_assigned = $user_id and is_accepted =0 and is_complete=0 ")->result_array();
        if (count($result_data) > 0) {
            $output["success"] = TRUE;
            $output["message"] = "A task is assign to you ";
            $output["task"] = $result_data;
        } else {

            $output["message"] = "no task assign";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function assign_task_to_user() {
        $output = array();
        if (count($_POST) > 0) {
            $data["task_assign_dept "] = $dept = $this->input->post("department");
            $data["is_assigned "] = $assign_user_id = $this->input->post("assign_user_id");
            $data[" created_by_user_id 	"] = $assignee_user_id = $this->input->post("assignee_user_id");
            $data["task_description"] = $task_desc = $this->input->post("task_desc");
            $data["priority"] = $task_priority = $this->input->post("task_priority");
            date_default_timezone_set("Asia/Calcutta");
            $data['created_date '] = $date = date('Y-m-d H:i:s');
            $task_id = $this->Common->addData("tasks", $data);
            if ($task_id > 0) {
                $output["success"] = TRUE;
                $output["message"] = "Task assign successfully";
            } else {
                $output["success"] = FALSE;
                $output["message"] = "SOME PROBLEM OCCOURED";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "no data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function get_user_info_according_dept() {
        $output = array();
        if (count($_POST) > 0) {
            $dept = $this->input->post("department");
            if ($dept == "" || $dept == NULL) {
                $output["success"] = false;
                //$output["message"] = "Please Enter Email";
                echo json_encode($output);
                exit;
            }
            $result_data = $this->Common->getSelectData("user_id,u_firstname,u_lastname", 'users', " u_dept_type='$dept' and is_active=1 ")->result_array();
            if ($result_data > 0) {
                $output["user_data_according_dept"] = $result_data;
                $output["success"] = TRUE;
                $output["message"] = "User-info according to department fetch successfully";
            } else {
                $output["success"] = FALSE;
                $output["message"] = "no data found";
            }
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function userRegistration() {//done
//u_email,u_firstname,u_lastname,u_dept_type,login_string
        $output = array();
        if (count($_POST) > 0) {
            $user_details["u_email"] = $email = $this->input->post("u_email");
            $user_details["u_firstname"] = $firstname = $this->input->post("u_firstname");
            $user_details["u_lastname"] = $lastname = $this->input->post("u_lastname");
            $user_details["u_dept_type"] = $dept_name = $this->input->post("u_dept_type");
            if ($email == "" || $email == NULL) {
                $output["success"] = false;
                $output["message"] = "Please Enter Email";
                echo json_encode($output);
                exit;
            }
            if ($firstname == "" || $firstname == NULL) {
                $output["success"] = false;
                $output["message"] = "Please Enter Firstname";
                echo json_encode($output);
                exit;
            }
            $where = "u_email='" . $user_details["u_email"] . "'";
            $retval = $this->Common->getData("users", $where)->row();
            if (count($retval) > 0) {
                $output["success"] = false;
                $output["message"] = "Email Address already exists";
                echo json_encode($output);
                exit;
            }

            $user_details["u_password"] = $this->Common->encryptPassword($this->input->post("login_string"));
            date_default_timezone_set("Asia/Calcutta");
            $user_details["created_date"] = date("Y-m-d H:i:s");
//          $user_details["is_active"] = 1;
            $user_id = $this->Common->addData("users", $user_details);
            $where = "user_id='" . $user_id . "'";
            $result_data = $this->Common->getData("users", $where)->result_array();
            $data = array(
                'user_data' => $result_data
            );
            $this->session->set_userdata($data);
//          $this->load->library('email');
//          $this->email->set_newline("\r\n");
//          $this->email->from('Gatha@gmail.com', 'Gatha');
//          $this->email->to($result_data[0]["u_email"]);
//          $this->email->subject('Gatha :your registration successfull');
////        $data["username"] = $email;
////        $data["password"] = $new_password;
//          $this->email->message($this->load->view("email_template/registration", true));
//          $this->email->send();
            $output["user_details"] = $result_data;
            $output["success"] = TRUE;
            $output["message"] = "Account created successfully";
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function userLogin() {//done
        $output = array();
        if (count($_POST) > 0) {
            $email_id = $this->input->post("u_email");
            $login_string = $this->input->post("login_string");
            $user_data = $this->Common->getData("users", "u_email = '$email_id' and is_active = 1")->result_array();
            if (count($user_data) == 1) {
                $user_id = $user_data[0]["user_id"];
                $output = $this->Common->checkGetLoginAccount($user_id);
                if ($user_data[0]["u_password"] == $this->Common->encryptPassword($login_string)) {
                    $data = array(
                        'user_data' => $user_data
                    );
                    $this->session->set_userdata($data);
                    //$output['userdata']=$user_data;
                    echo json_encode($output);
                    exit;
                } else {
                    $output = array();
                    $output["success"] = false;
                    $output["message"] = "Invalid Email Address or password!";
                    echo json_encode($output);
                    exit;
                }
            } else if (count($user_data) > 1) {
                $output["success"] = false;
                $output["message"] = "Invalid Account! Contact administrators";
                echo json_encode($output);
                exit;
            } else {
                $output["success"] = false;
                $output["message"] = "Invalid username or password";
                echo json_encode($output);
                exit;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//
    function Updatetask() {//done
        $output = array();
        if (count($_POST) > 0) {

            $assigndept = $this->input->post('assigndept');
            $priority = $this->input->post('priority');
            $Task_desc = $this->input->post('task_description');
            $task_id = $this->input->post('task_id');
            $update = array(
                "task_assign_dept" => $assigndept,
                "priority " => $priority,
                "task_description " => $Task_desc,
            );
            $where = "task_id=$task_id";
            $data = $this->Common->update("tasks", $update, $where);
            if (count($data) > 0) {
                $output["success"] = TRUE;
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No input data found";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function gettask() {//done ,for edit 
        $output = array();
        if (count($_POST) > 0) {
            $task_id = $this->input->post('task_id');
            $where = "task_id=$task_id ";
            //$data=[];
            $data = $this->Common->getSelectData("task_description,task_id,task_assign_dept,priority ", "tasks", $where)->row();
            //  print_r($data->task_description);
            //  exit;
            if (count($data) > 0) {
                $output['task'] = $data;
                $output["success"] = TRUE;
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function completetask() {//done
        if (count($_POST) > 0) {
            $task_id = $this->input->post('task_id');
            $u_id = $this->input->post('u_id');
            $data['is_complete'] = 1;
            $data1['is_complete'] = 1;
            date_default_timezone_set("Asia/Calcutta");
            $data1["completed_date "] = date("Y-m-d H:i:s");
            $emailss = $this->Common->getSelectData("u_email", "users", "user_id=$u_id")->row();
            $data['completed_by '] = $emailss->u_email;

            $this->Common->update("tasks", $data, "task_id=$task_id ");

            $this->Common->update("taskmanage", $data1, "task_id=$task_id ");

            $output["success"] = TRUE;
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($output));
        }
    }

    function createTask() {//done
        $output = array();
        if (count($_POST) > 0) {
            $user_id = $this->input->post('user_id');
            $assigndept = $this->input->post('assigndept');
            $priority = $this->input->post('priority');
            $task_desc = $this->input->post('task_desc');
            date_default_timezone_set("Asia/Calcutta");
            $date = date('Y-m-d H:i:s');
            $task_details["created_by_user_id"] = $user_id;
            $task_details["task_assign_dept"] = $assigndept;
            $task_details["priority"] = $priority;
            $task_details["task_description"] = $task_desc;
            $task_details["created_date"] = $date;
            $task_data = $this->Common->addData("tasks", $task_details);

//              
            if ($task_data > 0) {
                $output["success"] = TRUE;
                $output["message"] = "Insert Successfully";
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No data inserted";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

//    function signup() {
//        if (isset($_SESSION['Email']) && $_SESSION['Email'] != "") {
//            redirect("welcome/index");
//        } else {
//            $firstname = $this->input->post('user_firstame');
//            $lastname = $this->input->post('user_lastname');
//            $email_id = $this->input->post('user_email');
//            $password = $this->input->post('user_password');
//            //print_r($_POST);
//
//            $data = $this->Model_signin->InsertData($firstname, $lastname, $email_id, $password);
//            if ($data > 0) {
//                $sessionData = array(
//                    'Firstname' => $firstname,
//                    'Lastname' => $lastname,
//                    'Email' => $email_id,
//                );
//                $this->session->set_userdata($sessionData);
//                echo "correct";
//            } else {
//                echo 'wrong';
//            }
//        }
//    }
//    function userdata() {//echo hello;
//        //exit;
//        if (isset($_SESSION['Email']) && $_SESSION['Email'] != "") {
//            $data['check'] = $this->Model_signin->getuserData();
//            $data['check1'] = $this->session->userdata();
//
//            // print_r($data);
//            // exit;
////         print json_encode($data
//            $this->output->set_content_type('application/json');
//            $this->output->set_output(json_encode($data));
//        } else {
//            //echo 'wrong';
//            $this->load->view('login');
//        }
//    }
    function completedTaskData() {
        $output = array();
        if (count($_POST) > 0) {
            $user_id = $this->input->post("user_id");
            $output = array();

            $join_cond = "tasks.task_id = taskmanage.task_id";
            $where = "taskmanage.accepted_by_user_id = $user_id and taskmanage.is_complete = 1 and taskmanage.is_cancel = 0 and tasks.is_complete = 1 and tasks.is_accepted = 1";
            $task_data = $this->Common->getJoinedData("tasks.task_id,tasks.task_description ,tasks.priority ,tasks.created_date ,taskmanage.accepted_date ,taskmanage.completed_date ", 'tasks', 'taskmanage', $join_cond, $where)->result_array();

            if (count($task_data) > 0) {

                $output["success"] = TRUE;
                $output["task_data"] = $task_data;
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No data found";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getActiveTasks() {
        $output = array();

        $join_cond = "tasks.created_by_user_id = users.user_id";
        $where = "is_complete = 0 and  	is_assigned =0";
        $task_data = $this->Common->getJoinedData("tasks.*,users.u_email,users.u_firstname,users.u_lastname,users.u_dept_type", 'tasks', 'users', $join_cond, $where)->result_array();

        if (count($task_data) > 0) {

//            for($i=0;$i<count($task_data);$i++){
//             $user_id=  $task_data[$i]['created_by_user_id']; 
//             $where = "user_id =$user_id";
//              $data= $this->Common->getData('users', $where)->result_array(); 
//             
//              $name= $data[0]['u_firstname']." ".$data[0]['u_lastname'];
//           $task_data[$i]['name']=$name;
//            }
            $output["success"] = TRUE;
            $output["task_data"] = $task_data;
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function taskaccept() {
        $output = array();
        if (count($_POST) > 0) {
            $data['accepted_by_user_id'] = $user_id = $this->input->post("user_id");
            $data['task_id '] = $task_id = $this->input->post("task_id");
            date_default_timezone_set("Asia/Calcutta");
            $data['accepted_date'] = $date = date('Y-m-d H:i:s');
            $data = $this->Common->addData("taskmanage", $data);
            $update = array(
                "is_accepted" => 1,
            );
            $where = "task_id=$task_id";
            $data = $this->Common->update("tasks", $update, $where);
            if (count($data) > 0) {
                $output["success"] = TRUE;
                $output["message"] = "Insert Successfully";
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No data inserted";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function canceltask() {
        $output = array();
        if (count($_POST) > 0) {
            $task_id = $this->input->post("task_id");
            $user_id = $this->input->post("user_id");
            $data['comments'] = $comment = $this->input->post("comment");
            $data['is_cancel '] = 1;
            $data1['is_accepted  '] = 0;
            $this->Common->update("taskmanage", $data, "task_id=$task_id and taskmanage.accepted_by_user_id=$user_id and taskmanage.is_cancel=0 ");
            $this->Common->update("tasks", $data1, "task_id=$task_id ");
            $output["success"] = TRUE;
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

    function getAcceptedTasks() {
        $output = array();
        if (count($_POST) > 0) {
            $user_id = $this->input->post("user_id");
            if ($user_id == "" || $user_id == NULL) {
                $output["success"] = false;
                $output["message"] = "Please enter User id";
                echo json_encode($output);
                exit;
            }

            $join_cond = "tasks.task_id = taskmanage.task_id";
            $where = "taskmanage.accepted_by_user_id = $user_id and taskmanage.is_complete = 0 and taskmanage.is_cancel = 0 and tasks.is_complete = 0 and tasks.is_accepted = 1";
            $acceptd_task = $this->Common->getJoinedData("tasks.*,taskmanage.accepted_date", 'tasks', 'taskmanage', $join_cond, $where)->result_array();
            if (count($acceptd_task) > 0) {
                $output["success"] = TRUE;
                $output["accept_task_data"] = $acceptd_task;
            } else {
                $output["success"] = FALSE;
                $output["message"] = "No data found";
            }
        } else {
            $output["success"] = FALSE;
            $output["message"] = "No input data found";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($output));
    }

}
