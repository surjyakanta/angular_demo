<?php

class Common extends CI_Model {

    function escape($val) {
        $mysql_real_escape_string = mysqli_real_escape_string(get_connection(), $val);
        return $mysql_real_escape_string;
    }

    function encryptPassword($password) {
        return md5($password);
    }

    function encryptWithBase64_encode($object) {
        return base64_encode($object);
    }

    function encryptWithBase64_decode($object) {
        return base64_decode($object);
    }

    function addData($tablename = "", $data = array()) {
        $this->db->insert($tablename, $data);
        return $this->db->insert_id();
    }

    function addDataBatch($tablename = "", $data = array()) {
        $val = $this->db->insert_batch($tablename, $data);
        return $this->db->insert_id();
    }

    function getData($tablename = "", $where = "") {
        $this->db->where("$where", null, FALSE);
        $val = $this->db->get($tablename);
//        echo $this->db->last_query();
        // exit();
        return $val;
    }

    function getDataGroupBy($tablename = "", $where = "") {
//        $query = $this->db->query("SELECT DISTINCT utility_id,utility_name,Rrate2015,Crate2015,Irate2015 FROM $tablename where $where order by utility_id");
        $query = $this->db->query("SELECT * FROM $tablename where $where group by utility_id");
        return $query;
    }

    function getDataLimit($tablename = "", $where = "", $orderby = "", $limit, $offset = 0, $isAsc = 0) {
        $this->db->where("$where", null, FALSE);
        if ($isAsc == 1) {
            $this->db->order_by($orderby, "ASC");
        } else {
            $this->db->order_by($orderby, "DESC");
        }

        $val = $this->db->get($tablename, $limit, $offset);
        //echo $this->db->last_query();
        return $val;
    }

    function getAllData($tablename = "") {
        $val = $this->db->get($tablename);
        //echo $this->db->last_query();
        return $val;
    }

    function getCountSurvey($survey_field_column, $table_name) {
        $query = $this->db->query("SELECT $survey_field_column,count(*) FROM `$table_name` group by $survey_field_column");
        return $query;
    }

    function getDataOrderBy($tablename = "", $where = "", $orderby = "") {
//        $this->db->where("$where", null, TRUE);
        $this->db->order_by($orderby, "DESC");
        $val = $this->db->get($tablename);
        return $val;
    }

    function getDataOrderByWithWhere($tablename = "", $where = "", $orderby = "") {
        $this->db->where("$where", null, TRUE);
        $this->db->order_by($orderby, "DESC");
        $val = $this->db->get($tablename);
        return $val;
    }

    function getDataOrderByWithWhereASC($tablename = "", $where = "", $orderby = "") {
        $this->db->where("$where", null, TRUE);
        $this->db->order_by($orderby, "ASC");
        $val = $this->db->get($tablename);
        return $val;
    }

    function getDataOrderByWithSelect($select = "", $tablename = "", $where = "", $orderby = "") {
        $this->db->select($select, FALSE);
        $this->db->where("$where", null, TRUE);
        $this->db->order_by($orderby, "DESC");
        $val = $this->db->get($tablename);
        return $val;
    }

    function getDataOrderByWithSelectWithCase($select = "", $tablename = "", $where = "", $orderby = "", $case = "") {
        $this->db->select($select . $case, FALSE);
        $this->db->where("$where", null, TRUE);
        $this->db->order_by($orderby, "DESC");
        $val = $this->db->get($tablename);
        return $val;
    }

    function getRecordByComma($tablename = "", $colName, $where) {
        $this->db->select('group_concat( ' . $colName . ' SEPARATOR ",") as r_id', FALSE);
        $this->db->from($tablename);
        $this->db->where($where, NULL, FALSE);
        return $this->db->get();
    }

    function trim_city() {
        $cities = $this->db->query("SELECT distinct(city) FROM `zipcodes` where trim_city = ''");
        $count = 0;
        foreach ($cities->result() as $row) {
            $update["trim_city"] = str_replace(" ", "", $row->city);
            $where = "city='" . $row->city . "'";
            $this->db->where("city", $row->city);
            $this->db->update("zipcodes", $update);
            echo $count++;
        }
        echo str_replace(" ", "", "Aguas Buenas");
    }

    function update($tablename = "", $data = array(), $where = "") {
        $this->db->where("$where", null, false);
        return $this->db->update($tablename, $data);
        // echo $this->db->last_query();
    }

    function delete($tablename = "", $where = "") {
        $this->db->where("$where", null, false);
        $this->db->delete($tablename);
    }

    function deleteUsingJoin($table = "", $joinTable, $joinCond = "", $where) {
        $query = "delete e FROM $table e join $joinTable on $joinCond WHERE $where";
        $this->db->query($query);
    }

    function printLatLang($address) {
        $prepAddr = str_replace(' ', '+', $address);
        $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false');
        $output = json_decode($geocode);
        $data["longitude"] = $data["latitude"] = "";
        if ($output->status == "OK") {
            $latitude = $output->results[0]->geometry->location->lat;
            $longitude = $output->results[0]->geometry->location->lng;
            $data["latitude"] = $latitude;
            $data["longitude"] = $longitude;
            $data["isOK"] = true;
        } else {
            $data["isOK"] = false;
        }
        return $data;
    }

    function getJoinedData($select, $table, $joinedTable, $joinCond, $where) {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        $this->db->join($joinedTable, $joinCond);
        $this->db->where($where, null, FALSE);
        return $this->db->get();
    }

    function getJoinedData2($select, $table, $joinedTable, $joinCond, $joinedTable2, $joinCond2, $where) {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        $this->db->join($joinedTable, $joinCond);
        $this->db->join($joinedTable2, $joinCond2);
        $this->db->where($where, null, FALSE);
        return $this->db->get();
    }

    function getSelectData($select, $tablename, $where) {
        $this->db->select($select, FALSE);
        $this->db->from($tablename);
        $this->db->where($where, null, false);
        return $this->db->get();
    }

    function getSelectedAllData($select, $tablename) {
        $this->db->select($select, FALSE);
        $this->db->from($tablename);
        return $this->db->get();
    }

    function getSelectedAllDataOrderBy($select, $tablename, $orderBy) {
        $this->db->select($select, FALSE);
        $this->db->from($tablename);
        $this->db->order_by($orderBy);
        return $this->db->get();
    }

    function calculatePercent($select, $tablename, $where) {
        $result_data = $this->getSelectData($select, $tablename, $where);
        $isEmpty = 0;
        $column = explode(",", $select);
        foreach ($result_data->result() as $r) {
            for ($i = 0; $i < count($column); $i++) {
                if ($r->$column[$i] == "") {
                    $isEmpty++;
                }
            }
        }
        $percent = (int) (((count($column) - $isEmpty) / count($column)) * 100);
        return $percent;
    }

    function addUpdateData($tablename = "", $data = array(), $where = "") {
        $this->db->where("$where", null, FALSE);
        $ret = $this->db->get($tablename);
        if ($ret->num_rows() > 0) {
            $this->db->where("$where", null, FALSE);
            $this->db->update($tablename, $data);
        } else {
            $this->db->insert($tablename, $data);
        }
        //return $this->db->insert_id();
    }

    function getAddData($tablename = "", $data = array(), $where = "") {
        $this->db->where("$where", null, FALSE);
        $ret = $this->db->get($tablename);
        if ($ret->num_rows() > 0) {
            return $ret;
        } else {
            $this->db->insert($tablename, $data);
            $this->db->where("$where", null, FALSE);
            return $this->db->get($tablename);
        }
    }

    function getCount($select, $tablename) {
        $this->db->select($select);
        $this->db->from($tablename);
        $ret_val = $this->db->get();
        return $ret_val;
    }

    function encryptFunction($string) {
        //$string = $this->input->post("string");
        $this->load->library('encrypt');
        $output["encrypt_string"] = $encrypt_string = strrev($this->encrypt->encode($string));
        //$output["decrypt_value"] =$this->encrypt->decode($encrypt_string);
        return $encrypt_string;
    }

    function deviceRegistration($aw_id, $device_id, $unique_key, $platform, $version) {
        $registaration["aw_id"] = $aw_id;
        $registaration["device_id"] = $device_id;
        $registaration["unique_key"] = $unique_key;
        $registaration["platform"] = $platform; //1- ios, 2- android, 3- web
        $registaration["version"] = $version;
        $registaration["created_date"] = date("Y-m-d H:i:s");
//        $registaration["registration_date"] = date("Y-m-d H:i:s");
//        $registaration["updated_date"] = date("Y-m-d H:i:s");
//        $this->Common->addUpdateData("users_device_registration ", $registaration, "user_id = $user_id");
        $this->addUpdateData("notification_device_registration", $registaration, "unique_key='$unique_key'");
    }

    function deviceRegistrationOld($aw_id, $device_id, $platform, $version) {
        $registaration["aw_id"] = $aw_id;
        $registaration["device_id"] = $device_id;
//        $registaration["unique_key"] = $unique_key;
        $registaration["platform"] = $platform; //1- ios, 2- android, 3- web
        $registaration["version"] = $version;
        $registaration["created_date"] = date("Y-m-d H:i:s");
//        $registaration["registration_date"] = date("Y-m-d H:i:s");
//        $registaration["updated_date"] = date("Y-m-d H:i:s");
//        $this->Common->addUpdateData("users_device_registration ", $registaration, "user_id = $user_id");
        $this->addUpdateData("notification_device_registration", $registaration, "device_id='$device_id'");
    }

    function checkGetLoginAccount($aw_id) {
        $output = array();
//        $this->db->select("u.aw_id, u.u_name, u.u_username, u.u_firstname, u.u_lastname, u.u_email, u.isDummyPassword, u.u_avatar_pic, u.login_with, u.u_zip, u.latitude, u.longitude, u.state, u.state_name, u.city, u.address_line1, u.address_line2 ");
        $this->db->select("*");
        $this->db->from("users u");
        $this->db->where("u.user_id", $aw_id);
        $this->db->where("u.is_active", "1");
        $user_details = $this->db->get();

        if (count($user_details) == 1) {
            $output["success"] = true;
            $output["user_details"] = $user_details->result_array();
//            $output["user_details"] = $user_details->row();
            $output["message"] = "Login successfully";
            return $output;
        }
    }

    function checkGetUpdatedAccount($aw_id) {
//        $output = array();
//        $this->db->select("u.aw_id, u.u_name, u.u_username, u.u_firstname, u.u_lastname, u.u_email, u.isDummyPassword, u.u_avatar_pic, u.login_with, u.u_zip, u.latitude, u.longitude, u.state, u.state_name, u.city, u.address_line1, u.address_line2 ");
        $this->db->select("*");
        $this->db->from("users u");
        $this->db->where("u.aw_id", $aw_id);
        $this->db->where("u.u_active", "1");
        $user_details = $this->db->get();

        if (count($user_details) == 1) {
//            $output["success"] = true;
//            $output["user_details"] = $user_details->row();
//            $output["message"] = "Login successfully";
            return $user_details->result_array();
        }
    }

    function zipcode_data($date_new_print) {
        $query = (hfgh);
        return $query;
    }

    function format_phone($phone) {
        $phone = preg_replace("/[^0-9]/", "", $phone);
        if (strlen($phone) == 7)
            return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
        elseif (strlen($phone) == 10)
            return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
        else
            return $phone;
    }

    function social_media_login($email_id, $login_string, $login_with) {
        if ($login_string == "") {
            $login_string = "/";
        }
        $data = array();
        $user_data = $this->Common->getData("users", "u_email = '$email_id' and u_active = 1")->result_array();
        if (count($user_data) == 1) {
            $user_id = $user_data[0]["user_id"];
            if ($login_with == 2) { //fb
                if ($user_data[0]["fb_id"] == $login_string) {
                    $update = array();
                    $update["u_last_login"] = date("Y-m-d H:i:s");
                    $this->update("users", $update, "user_id = $user_id");
                    $output = $this->checkGetLoginAccount($user_id);

                    if ($this->session->userdata("search_string") != '') {
                        $output['search_string'] = $this->session->userdata("search_string");
                    }
                    $output["current_latitude"] = $this->session->userdata("current_latitude");
                    $output["current_longitude"] = $this->session->userdata("current_longitude");
                    $output["current_timezone"] = $this->session->userdata("current_timezone");

                    $array_items = array('current_latitude' => '', 'current_longitude' => '', 'current_timezone' => '', 'success' => '');
                    $this->session->unset_userdata($array_items);

                    $this->authweb->login($output);

                    $output["success"] = true;
                    $output["message"] = "";
                    return $output;
                } else if ($user_data[0]["fb_id"] == "") {
                    $update = array();
                    $update["u_last_login"] = date("Y-m-d H:i:s");
                    $update["fb_id"] = $login_string;
                    $this->update("users", $update, "user_id = $user_id");
                    $output = $this->checkGetLoginAccount($user_id);

                    if ($this->session->userdata("search_string") != '') {
                        $output['search_string'] = $this->session->userdata("search_string");
                    }
                    $output["current_latitude"] = $this->session->userdata("current_latitude");
                    $output["current_longitude"] = $this->session->userdata("current_longitude");
                    $output["current_timezone"] = $this->session->userdata("current_timezone");

                    $array_items = array('current_latitude' => '', 'current_longitude' => '', 'current_timezone' => '', 'success' => '');
                    $this->session->unset_userdata($array_items);

                    $this->authweb->login($output);
                    $output["success"] = true;
                    $output["message"] = "";
                    return $output;
                } else {
                    $output["success"] = false;
                    $output["error_no"] = 9;
                    $output["message"] = "Invalid Facebook account linked with email id!";
                    return $output;
                }
            }
            if ($login_with == 4) { // linkedin
                if ($user_data[0]["linkedin_id"] == $login_string) {
                    $update = array();

                    $update["u_last_login"] = date("Y-m-d H:i:s");
                    $this->update("users", $update, "user_id = $user_id");
                    $output = $this->checkGetLoginAccount($user_id);

                    if ($this->session->userdata("search_string") != '') {
                        $output['search_string'] = $this->session->userdata("search_string");
                    }
                    $output["current_latitude"] = $this->session->userdata("current_latitude");
                    $output["current_longitude"] = $this->session->userdata("current_longitude");
                    $output["current_timezone"] = $this->session->userdata("current_timezone");

                    $array_items = array('current_latitude' => '', 'current_longitude' => '', 'current_timezone' => '', 'success' => '');
                    $this->session->unset_userdata($array_items);

                    $this->authweb->login($output);

                    $output["success"] = true;
                    $output["message"] = "";
                    return $output;
                } else if ($user_data[0]["linkedin_id"] == "") {
                    $update = array();
                    $update["linkedin_id"] = $login_string;
                    $update["u_last_login"] = date("Y-m-d H:i:s");
                    $this->update("users", $update, "user_id = $user_id");
                    $output = $this->checkGetLoginAccount($user_id);

                    if ($this->session->userdata("search_string") != '') {
                        $output['search_string'] = $this->session->userdata("search_string");
                    }
                    $output["current_latitude"] = $this->session->userdata("current_latitude");
                    $output["current_longitude"] = $this->session->userdata("current_longitude");
                    $output["current_timezone"] = $this->session->userdata("current_timezone");

                    $array_items = array('current_latitude' => '', 'current_longitude' => '', 'current_timezone' => '', 'success' => '');
                    $this->session->unset_userdata($array_items);

                    $this->authweb->login($output);
                    $output["success"] = true;
                    $output["message"] = "";
                    return $output;
                } else {
                    $output["success"] = false;
                    $output["error_no"] = 9;
                    $output["message"] = "Invalid Linkedin account linked with email id!";
                    return $output;
                }
            }

            if ($login_with == 3) { //gmail/google
                if ($user_data[0]["g_id"] == $login_string) {
                    $update = array();
                    $update["u_last_login"] = date("Y-m-d H:i:s");
                    $this->update("users", $update, "user_id = $user_id");
                    $output = $this->checkGetLoginAccount($user_id);

                    if ($this->session->userdata("search_string") != '') {
                        $output['search_string'] = $this->session->userdata("search_string");
                    }
                    $output["current_latitude"] = $this->session->userdata("current_latitude");
                    $output["current_longitude"] = $this->session->userdata("current_longitude");
                    $output["current_timezone"] = $this->session->userdata("current_timezone");

                    $array_items = array('current_latitude' => '', 'current_longitude' => '', 'current_timezone' => '', 'success' => '');
                    $this->session->unset_userdata($array_items);

                    $this->authweb->login($output);
                    $output["success"] = true;
                    $output["message"] = "";
                    return $output;
                } else if ($user_data[0]["g_id"] == "") {
                    $update = array();
                    $update["u_last_login"] = date("Y-m-d H:i:s");
                    $update["g_id"] = $login_string;
                    $this->update("users", $update, "user_id = $user_id");
                    $output = $this->checkGetLoginAccount($user_id);

                    if ($this->session->userdata("search_string") != '') {
                        $output['search_string'] = $this->session->userdata("search_string");
                    }
                    $output["current_latitude"] = $this->session->userdata("current_latitude");
                    $output["current_longitude"] = $this->session->userdata("current_longitude");
                    $output["current_timezone"] = $this->session->userdata("current_timezone");

                    $array_items = array('current_latitude' => '', 'current_longitude' => '', 'current_timezone' => '', 'success' => '');
                    $this->session->unset_userdata($array_items);

                    $this->authweb->login($output);
                    $output["success"] = true;
                    $output["message"] = "";
                    return $output;
                } else {
                    $output["success"] = false;
                    $output["error_no"] = 9;
                    $output["message"] = "Invalid Google account linked with email id!";
                    return $output;
                }
            }
        } else if (count($user_data) > 1) {
            $output["success"] = false;
            $output["message"] = "Invalid Account! Contact SG administrators";
            return $output;
        } else {

            $output["success"] = false;
            $output["message"] = "We do not have your account. Become a member!";
            return $output;
        }
    }

    function validateZipCodeUsingGoogleApi($zipcode) {
        $valZip = str_replace(" ", "", $zipcode);
        $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?components=postal_code:' . $valZip . '&sensor=false');
        $output = json_decode($geocode);
        $result = array();
        if ($output->status != "OK" && count($output->results) <= 0) {
            $result["success"] = FALSE;
            $result["message"] = "Zip Code is not valid";
        } else {
            $zip = ""; //$output->results[0]->address_components[0]->long_name;
            $city = "";
            $state = "";
            $addressComp = array();
            $addressComp = $output->results[0]->address_components;
            //print_r($addressComp);
            foreach ($addressComp as $key => $value) {
                $type = array();
                $type = $value->types;
                $array = json_decode(json_encode($type), true);
                if (in_array("postal_code", $array)) {
                    $zip = $value->long_name;
                } elseif (in_array("administrative_area_level_1", $array)) {
                    $state = $value->short_name;
                } elseif (in_array("locality", $array)) {
                    $city = $value->long_name;
                }
            }
            if ($valZip != $zip) {
                $result["success"] = FALSE;
                $result["message"] = "Zip Code is not valid";
            } else {
                $result["success"] = TRUE;
                $row = count($output->results[0]->address_components) - 2;
                $result["city_name"] = $city; //$output->results[0]->address_components[1]->long_name;
                $result["city_zipcode"] = $zip; // $output->results[0]->address_components[0]->long_name;
                $result["state_name"] = $state; //$output->results[0]->address_components[$row]->short_name;
                $result["latitude"] = $output->results[0]->geometry->location->lat;
                $result["longitude"] = $output->results[0]->geometry->location->lng;
            }
        }
        return $result;
    }

    function getAddressFromLatLong($latitude, $longitude) {
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBgCdUaSSClPpdI6c04lY3x476rwM7orlM&latlng=' . trim($latitude) . ',' . trim($longitude) . '&sensor=false';
        $json = @file_get_contents($url);
        $data = json_decode($json);
        $status = $data->status;
        if ($status == "OK" && count($data) > 0)
            return $data->results[0]->formatted_address;
        else
            return "";
    }

    function getJoinedSelectedDataWithHaving($select, $table, $having, $joinTable1, $joinCondition1, $joinTable2, $joinCondition2, $orderBy) {
        $this->db->select($select);
        $this->db->from($table);
        $this->db->join($joinTable1, $joinCondition1);
        $this->db->join($joinTable2, $joinCondition2);
        $this->db->having($having, NULL, FALSE);
        $this->db->order_by($orderBy, "desc", FALSE);
//        $this->db->limit($limit);
        return $this->db->get();
    }

    function getTimezonefromLatLng($lat, $lng) {
        $timestamp = strtotime(date('Y-m-d', time()) . '00:00:00');
        $url = 'https://maps.googleapis.com/maps/api/timezone/json?location=' . trim($lat) . ',' . trim($lng) . '&timestamp=' . $timestamp . '&key=AIzaSyBgCdUaSSClPpdI6c04lY3x476rwM7orlM';
        $json = @file_get_contents($url);
        $data = json_decode($json);
        $status = $data->status;
        if ($status == "OK" && count($data) > 0)
            return $data->timeZoneName;
        else
            return "";
    }

    function selectDataWithCase($select, $table) {
        $this->db->select("r.groups,case when r.groups = 'Heating' then '#FF3E2F' when r.groups = 'Cooling' then '#14D0AD' when r.groups = 'Hot Water' then '#929497' when r.groups = 'Large Appliances' then '#FFC708' when r.groups = 'Small Appliances' then '#A5CF0C' when r.groups = 'Lighting' then '#40B4DD' End as color1");
        $this->db->from("retrieve_details r");
        return $this->db->get();
//        CASE
//        WHEN a.Stage = 1 and a.YesorNo = 1 THEN 'No'
//        WHEN a.Stage = 1 and a.YesorNo = 0 THEN 'Yes'
//        END AS NewViewColumn
    }

    function getassigneeswithin_one_mile() {  //gatha...
//        Select  (((acos(sin((" . $event["latitude"] . "*pi()/180)) * sin((e.latitude*pi()/180))+cos((" . $event["latitude"] . "*pi()/180)) * cos((e.latitude*pi()/180)) * cos(((" . $event["longitude"] . "- e.longitude)* pi()/180))))*180/pi())*60*1.1515) as distance
//        $notify_host = $this->db->query("SELECT  (((acos(sin((" . $event["latitude"] . "*pi()/180)) * sin((e.latitude*pi()/180))+cos((" . $event["latitude"] . "*pi()/180)) * cos((e.latitude*pi()/180)) * cos(((" . $event["longitude"] . "- e.longitude)* pi()/180))))*180/pi())*60*1.1515) as distance from events e Having distance < 1.4908538070526 or (distance * 5280)= 500 ")->result_array();  //query notifying owner also
//             $query ="Select * from users ";
//             $user_data = $this->db->query($query);
        $query = $this->db->query("SELECT * FROM `users` ");
        $data = $query->result_array();
        $i = 0;
        $j = 0;
        if (count($data) > 0) {
            foreach ($data as $info) {
                $event["latitude"] = $info['latitude'];
                $event["longitude"] = $info['longitude'];
                $notify_host = $this->db->query("SELECT  (((acos(sin((" . $event["latitude"] . "*pi()/180)) * sin((e.latitude*pi()/180))+cos((" . $event["latitude"] . "*pi()/180)) * cos((e.latitude*pi()/180)) * cos(((" . $event["longitude"] . "- e.longitude)* pi()/180))))*180/pi())*60*1.1515) as distance,id,title,owner_id,e.parent_event_id from events e  join event_assignees ea on e.id=ea.event_id Having distance < 1.4908538070526 and  distance >0.9974602182852323")->result_array();  //query notifying owner also
                if (count($notify_host) > 0) {
                    $notify_host12[$i] = $notify_host[0];
                    $i++;
                }
            }
        }
        $notify_new = $notify_host12;

        if (count($notify_new) > 0) {
            foreach ($notify_new as $value) {
                $notify_msg = " Is your gatha’ring '" . $value["title"] . "' still going?.";
                $details = array();
                $details["event_owner_id"] = $value["owner_id"];
                $details["id"] = $value["id"];
                $details["parent_event_id"] = $value["parent_event_id"];
                $update['status_notification'] = 1;
                $this->Common->update("events", $update, "id=" . $value["id"]);
                $this->Common->sendPushMessages($value["owner_id"], $notify_msg, "9", $details, 0);
            }
        }
        $notify_host_for500feet = array();
        if (count($data) > 0) {
            foreach ($data as $info) {
                $event["latitude"] = $info['latitude'];
                $event["longitude"] = $info['longitude'];
                $notify_host11 = $this->db->query("SELECT  (((acos(sin((" . $event["latitude"] . "*pi()/180)) * sin((e.latitude*pi()/180))+cos((" . $event["latitude"] . "*pi()/180)) * cos((e.latitude*pi()/180)) * cos(((" . $event["longitude"] . "- e.longitude)* pi()/180))))*180/pi())*60*1.1515) as distance from events e Having (distance * 5280)= 500 ")->result_array();
                if (count($notify_host11) > 0) {
                    $notify_host_for500feet[$j] = $notify_host;
                    $j++;
                }
            }
        }
        print_r($notify_host_for500feet);
        exit;
        if (count($notify_host_for500feet) > 0) {
            foreach ($notify_host_for500feet as $value) {
                $notify_msg = " Is your gatha’ring '" . $value["title"] . "' still going?.";
                $details = array();
                $details["event_owner_id"] = $value["owner_id"];
                $details["id"] = $value["id"];
                $details["parent_event_id"] = $value["parent_event_id"];
                $this->Common->sendPushMessages($value["owner_id"], $notify_msg, "9", $details, 0);
            }
        }
    }

}
