<?php
/*
  Developer:  Ehtesham Mehmood
  Site:       PHPCodify.com
  Script:     Angularjs Login Script using PHP MySQL and Bootstrap
  File:       index.php
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex">
        <title>sign up</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    </head>
    <body ng-app="AngularJSsignup" ng-controller="AngularsignupController as angCtrl">
        <div class="container" ng-mousedown="errorMsg = false;">
            <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title"> Sign up</div>
                    </div>
                    <div style="padding-top:30px" class="panel-body">
                        <form name="login" ng-submit="angCtrl.signupForm()"  class="form-horizontal" method="POST">
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon">Firstname</span>
                                <input type="text" id="inputfirstname" class="form-control" required autofocus ng-model="angCtrl.inputData.Firstname" placeholder="Firstname">
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon">Lastname</span>
                                <input type="text" id="inputlastname" class="form-control" required autofocus ng-model="angCtrl.inputData.Lastname" placeholder="Lastname">
                            </div>

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon">Email</span>
                                <input type="email" id="inputemail" class="form-control" required autofocus ng-model="angCtrl.inputData.email" placeholder="Email">
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon">Confirm Email</span>
                                <input type="email" id="inputemail" class="form-control" required autofocus ng-model="angCtrl.inputData.email1" placeholder="Confirm Email">
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon">Department</span>
                                <select name="dept" style="width:410px;" class="form-control" ng-model="selectedName" ng-options="item for item in names" required="">
                                    <option value="">Select Department</option> 
                                </select>
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon">Password</span>
                                <input type="password" id="inputpassword" class="form-control" required ng-model="angCtrl.inputData.password" placeholder="Password">
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon">Confirm Password</span>
                                <input type="password" id="inputpassword" class="form-control" required ng-model="angCtrl.inputData.password1" placeholder="Confirm Password">
                            </div>
                            <div class="form-group">
                                <!-- Button -->
                                <div class="col-sm-6 controls">
                                    <button type="submit" class="btn btn-primary pull-left"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Register</button>
                                </div>
                                <div class="col-sm-6 controls">
                                    <a href="<?php echo BASEURL . 'web/Welcome/login' ?>"><button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Login</button></a>
                                </div>
                            </div>
                            <div class="alert" ng-show="errorMsg" ng-style="myObj">
                                <span class="glyphicon glyphicon-hand-right"></span>&nbsp;&nbsp;{{errorMsg}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
                    angular.module('AngularJSsignup', []).controller('AngularsignupController', ['$scope', '$http', function ($scope, $http) {

                            $scope.names = ["ANDROID", "IOS", "PHP"];
                            this.signupForm = function () {
                                $scope.myObj = {
                                    "color": "white",
                                    "background-color": "red"
                                };
                                if (this.inputData.email1 != this.inputData.email) {
                                    $scope.errorMsg = "Email and Confirm Email does not match!";
                                    return false;
                                }
                                if (this.inputData.password1 != this.inputData.password) {
                                    $scope.errorMsg = "Password and Confirm Password does not match!";
                                    return false;
                                }
                                if ($scope.selectedName == 'undefned' || $scope.selectedName == '') {
                                    $scope.errorMsg = "Please Select Department";
                                    return false;
                                }
                                var user_data = 'u_firstname=' + this.inputData.Firstname + '&u_lastname=' + this.inputData.Lastname + '&u_email=' + this.inputData.email + '&login_string=' + this.inputData.password + '&u_dept_type=' + $scope.selectedName;
                                $http({
                                    method: 'POST',
                                    url: 'http://localhost/angular_js/api/Welcome/userRegistration',
                                    data: user_data,
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                }).success(function (data) {
                                    if (data.success == true) {
                                        $scope.myObj = {
                                            "color": "white",
                                            "background-color": "green"
                                        }
                                        $scope.errorMsg = data.message;
                                        window.location.href = 'http://localhost/angular_js/web/Welcome/index';
                                    } else {
                                        $scope.errorMsg = data.message;
                                    }
                                })
                            };
                        }]);
        </script>

    </body>
</html>


