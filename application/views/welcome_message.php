 <html ng-app="fetch">
    <head>
    <title>AngularJS GET request with PHP</title>
      <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular.min.js"></script>
    </head>

    <body style="background-color: #f0f4c3">
    <br>
      <div class="row">
          <div class="container" style="background-color: #fffde7">
         
          <div ng-controller="dbCtrl">
               <h3>  {{data1.Firstname}} {{data1.Lastname}}   </h3>
               <input type="text" ng-model="searchFilter" class="form-control" placeholder="search">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID </th>
                        <th>FIRST NAME</th>
                          <th>LAST NAME</th>
                        <th>EMAIL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="users in data | filter:searchFilter">
                        <td>{{users.id}}</td>
                        <td>{{users.Firstname}}</td>
                        <td>{{users.Lastname}}</td>
                        <td>{{users.email}}</td>
                        
                    </tr>
                    
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </body>

    <script>
        var fetch = angular.module('fetch', []);

        fetch.controller('dbCtrl', ['$scope', '$http', function ($scope, $http) {
          //  $http.get("ajax.php")
            $http({
				method: 'POST',
				url: 'userdata',
				//data: user_data,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			})
                .success(function(r){
                    $scope.data = r['check'];
                    $scope.data1=r['check1'];
                })
                .error(function() {
                    $scope.data = "error in fetching data";
                });
        }]);

    </script>

    </html>