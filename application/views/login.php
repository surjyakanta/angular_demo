<?php
/*
  Developer:  Ehtesham Mehmood
  Site:       PHPCodify.com
  Script:     Angularjs Login Script using PHP MySQL and Bootstrap
  File:       index.php
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex">
        <title>log in</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    </head>
    <body ng-app="AngularJSLogin" ng-controller="AngularLoginController as angCtrl" style="background-color: #fff8e1">
        <div class="container" ng-mousedown="errorMsg = false;">
            <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title"> Login</div>
                    </div>

                    <div style="padding-top:30px" class="panel-body" >
                        <form name="login" ng-submit="angCtrl.loginForm()" class="form-horizontal" method="POST">
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon">Email</span>
                                <input placeholder="Email" type="email" id="inputemail" class="form-control" required autofocus ng-model="angCtrl.inputData.email">
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon">Password</span>
                                <input placeholder="Password" type="password" id="inputpassword" class="form-control" required ng-model="angCtrl.inputData.password">
                            </div>
                            <div class="form-group">
                                <!-- Button -->
                                <div class="col-sm-6 controls">
                                    <button type="submit" class="btn btn-primary pull-left"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Login</button>
                                </div>
                                <div class="col-sm-6 controls">
                                    <a href="<?php echo BASEURL . 'web/Welcome/register' ?>"><button type="button" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Register</button></a>
                                </div>
                            </div>
                            <div class="alert" ng-show="errorMsg" ng-style="myObj">
                                <span class="glyphicon glyphicon-hand-right"></span>&nbsp;&nbsp;{{errorMsg}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
                    angular.module('AngularJSLogin', []).controller('AngularLoginController', ['$scope', '$http', function ($scope, $http) {
                            this.loginForm = function () {
                                var user_data = 'u_email=' + this.inputData.email + '&login_string=' + this.inputData.password;
                                $http({
                                    method: 'POST',
                                    url: 'http://localhost/angular_js/api/Welcome/userLogin',
                                    data: user_data,
                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                }).success(function (data) {
                                    if (data.success == true) {
                                        $scope.myObj = {
                                            "color": "white",
                                            "background-color": "green"
                                        }
                                        $scope.errorMsg = data.message;
                                        window.location.href = 'http://localhost/angular_js/web/Welcome/index';
                                    } else {
                                        $scope.myObj = {
                                            "color": "white",
                                            "background-color": "red"
                                        }
                                        $scope.errorMsg = data.message;
                                    }
                                }).error(function () {
                                    $scope.data = "error in fetching data";
                                });
                            };
                        }]);
        </script>

    </body>
</html>
