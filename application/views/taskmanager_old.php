<html ng-app="fetch">
    <head>
        <title>Task Manager with PHP</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/1.3.1/ui-bootstrap-tpls.min.js"></script>
    </head>
    <?php
    $user_id = $rows[0]['user_id'];

//    echo '<pre>';
//    print_r($rows);
    ?>
    <body style="background-color: #f0f4c3">
        <div class ="" style="text-align: center;margin: auto;background-color: #e0f2f1;padding:20px; width: 80%;" ng-controller="dbCtrl">
            <div class="well" style="height:70px">
                <div style="float:left;"><span style="font-size: 25px;color:#7986cb;">Task Info:{{pageinfo}}</span></div>
                <div style="float:right;margin-bottom: 7px;"><input type="text" ng-model="searchFilter" class="form-control" placeholder="search"></div>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="logout()"  >log out</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="createtask_div()"  >Create Task</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="getCurrentTask()">Current Task</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="getCompletedTask(<?php echo $user_id ?>)">Completed Task</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="getmyTask(<?php echo $user_id ?>)">My Task</button>
            </div>
            <div ng-show = "IsVisible" style="position: relative;background-color:#efebe9;padding: 30px;z-index: 99999; margin: auto; text-align: center;" class="container">
                <h3>{{var_task}}</h3>
                <form name="login"  class="form-horizontal" method="POST">
                    <div style="margin-bottom: 25px;margin-left: 10%" class="input-group">
                        <span style="margin-left:25px" class="input-group-addon ">Assign to</span>
                        <select style="width:80%" class="form-control"ng-model="assigndept" ng-options="item for item in dept" ></select>
                        <span  class="input-group-addon">priority</span>
                        <select style="width:80%" class="form-control"ng-model="priority" ng-options="item for item in prior" ></select>
                    </div>
                    <div style="margin-bottom: 25px;margin-left: 10%" class="input-group">
                        <textarea rows="10" cols="145" placeholder="please enter your Task here" ng-model="Task"></textarea>
                    </div>
                    <div class="form-group" style="margin-bottom: 25px; margin-left: 9%;">
                        <div class="col-sm-5 controls" ng-show = "IsVisible1">
                            <button class="btn btn-primary pull-right" ng-click="createtask(<?php echo $user_id ?>)"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Submit</button>
                        </div>
                        <div class="col-sm-5 controls" ng-show = "IsVisible2">
                            <button  class="btn btn-primary pull-right" ng-click="Updatetask(task.task_id)"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Update</button>
                        </div>
                        <div class="col-sm-5 controls" >
                            <button  class="btn btn-primary pull-left" ng-click="ShowHide1()"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <div ng-show="comment_div" style="position: relative;background-color:#efebe9;padding: 30px;z-index: 99999; margin: auto; text-align: center;" class="container">

                <div style="margin-bottom: 25px;margin-left: 10%" class="input-group">
                    <textarea rows="10" cols="145" placeholder="To cancel the task ,please enter why you want to cancel the task" ng-model="comment" required autofocus></textarea>
                </div>
                <div class="col-sm-5 controls" >
                    <button  class="btn btn-primary pull-right" ng-click="canceltask(<?php echo $user_id ?>)" ng-show="comment"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Cancel</button>
                </div>
                <div class="col-sm-5 controls" >
                    <button  class="btn btn-primary pull-left" ng-click="cancel_cancel()"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Back</button>
                </div>



            </div>
            <table class="table table-hover" ng-show="isTable">
                <thead>
                    <tr>
                        <th>Task_id </th>
                        <th ng-show= "mytaskhead">Asignee_email</th>
                        <th ng-show= "mytaskhead">Asignee_name</th>
                        <th ng-show= "mytaskhead">Assignee_belongs_to_dept</th>
                        <th ng-show= "todept">Task_assign_to_dept</th>
                        <th>priority</th>
                        <th>Task</th>
                        <th>Assign_date</th>
                        <th ng-show= "acptdate">Accepted_date</th>
                        <th ng-show= "isCompleted">Completed_Date</th>
                        <th ng-show= "IsCurrent"></th>
                        <th ng-show= "IsCurrent1"></th>
                    </tr>
                </thead>
                <tbody>

                    <tr ng-repeat="users in data| filter:searchFilter">
                        <td>{{users.task_id}}</td>
                        <td ng-show= "mytaskhead">{{users.u_email}}</td>
                        <td ng-show= "mytaskhead">{{users.u_firstname}}&nbsp;{{users.u_lastname}}</td>
                        <td ng-show= "mytaskhead">{{users.u_dept_type}}</td>
                        <td ng-show= "todept">{{users.task_assign_dept}}</td>
                        <td>{{users.priority}}</td>
                        <td>{{users.task_description}}</td>
                        <td >{{users.created_date}}</td>
                        <td ng-show= "acptdate" >{{users.accepted_date}}</td>
                        <td ng-show= "isCompleted">{{users.completed_date}}</td>

                        <td ng-show= "mytaskbtn" ><button   style="border-radius: 15px;width: 130px; height: 30px; border: 1px solid black;background-color: green;color: white;" type=""  ng-click="completetask(users.task_id, '<?php echo $user_id ?>')">complete</button></td>
                        <td ng-show= "IsCurrent"><button   style="border-radius: 15px;width: 130px; height: 30px; border: 1px solid black;background-color: green;color: white;" type=""  ng-show="users.is_accepted == 0"	 ng-click="taskaccept(users.task_id, '<?php echo $user_id ?>')">Task Accept</button></td>
                        <td ng-show= "IsCurrent1"><button style="border-radius: 15px;width: 100px; height: 30px; border: 1px solid black;background-color: #25A0E6;color: white;" type="" ng-show="users.created_by_user_id == '<?php echo $user_id; ?>'" ng-click="getEdittask(users.task_id)">Edit Task</button></td>
                        <td ng-show= "mytaskbtn"><button   style="border-radius: 15px;width: 130px; height: 30px; border: 1px solid black;background-color: green;color: white;" type="" ng-click="taskcancel_div(users.task_id)">cancel</button></td>
                    </tr>
                </tbody>

            </table>


        </div>
        <script type="text/javascript">

            var fetch = angular.module('fetch', []);
            fetch.controller('dbCtrl', ['$scope', '$http', function ($scope, $http) {
            $scope.pageinfo = "Current task";
            $scope.acptdate = false;
            $scope.mytaskbtn = false;
            $scope.IsVisible = false;
            $scope.IsVisible3 = true;
            $scope.IsVisible1 = true;
            $scope.IsVisible2 = true;
            $scope.IsCurrent = true;
            $scope.IsCurrent1 = true;
            $scope.isTable = true;
            $scope.mytaskhead = true;
            $scope.todept = true;
            $scope.comment_div = false;
            $scope.dept = ["ANDROID", "IOS", "PHP"];
            $scope.prior = ["HIGH", "MEDIUM", "LOW"];
            //logout
            $scope.logout = function () {
            window.location.href = 'http://localhost/angular_js/web/Welcome/logout';
            }

            //canceltask
            $scope.canceltask = function (userid) {
            var user_data = 'task_id=' + $scope.taskcancel + '&comment=' + $scope.comment + '&user_id=' + userid;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/canceltask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            if (r.success == true){
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            } else {
            $scope.data = r.message;
            }
            })

            };
            //cancel cancel
            $scope.cancel_cancel = function () {
            $scope.comment_div = false;
            $scope.isTable = true;
            };
            //task cancel div
            $scope.taskcancel_div = function (taskid) {
            $scope.taskcancel = taskid;
            $scope.comment_div = true;
            $scope.isTable = false;
            };
            //getmytask function
            $scope.getmyTask = function (user) {//done
            $scope.data = {};
            $scope.pageinfo = "MY task";
           $scope.comment_div = false;
            $scope.acptdate = true;
            $scope.mytaskbtn = true;
            $scope.mytaskhead = false;
            $scope.isTable = true;
            $scope.isCompleted = false;
            $scope.IsCurrent1 = true;
            $scope.IsVisible = false;
            $scope.todept = false;
            $scope.IsCurrent = false;
            var user_data = 'user_id=' + user;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/getAcceptedTasks',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            if (r.success == true){
            $scope.data = r['accept_task_data'];
            } else {
            $scope.data = r.message;
            }
            }).error(function () {
            $scope.data = "error in fetching data";
            });
            };
            $scope.taskaccept = function(task_id, user_id){//done
            var user_data = 'task_id=' + task_id + '&user_id=' + user_id;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/taskaccept',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
            if (data.success == true) {
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            } else {
            $scope.errorMsg = "Invalid ";
            }
            });
            }

            $scope.Updatetask = function (user) {//done
            var user_data = '&assigndept=' + $scope.assigndept + '&priority=' + $scope.priority + '&task_description=' + $scope.Task + '&task_id=' + user;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/Updatetask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
            if (data.success == true) {
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            } else {
            $scope.errorMsg = "Invalid ";
            }
            });
            }

            $scope.createtask = function (user) {//done'

            var user_data = 'assigndept=' + $scope.assigndept + '&priority=' + $scope.priority + '&task_desc=' + $scope.Task + '&user_id=' + user;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/createtask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {

            if (data.success == true) {
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            } else {
            $scope.errorMsg = "Invalid Email and Password";
            }
            });
            }

            //  $http.get("ajax.php")

            $scope.createtask_div = function () {//done
            $scope.comment_div = false;
            $scope.pageinfo = "create task";
            $scope.var_task = "Create Task";
            $scope.isTable = false;
            $scope.asigneedept = "";
            $scope.assigndept = "";
            $scope.priority = "";
            $scope.Task = "";
            //If DIV is visible it will be hidden and vice versa.
            $scope.IsVisible2 = false;
            $scope.IsVisible1 = true;
            $scope.IsVisible3 = true;
            $scope.IsVisible = true;
            };
            $scope.ShowHide1 = function () {//done
            //If DIV is visible it will be hidden and vice versa.
            $scope.IsVisible3 = true;
            $scope.isTable = true;
            $scope.IsVisible = $scope.IsVisible ? false : true;
            };
            //edit task
            $scope.getEdittask = function (taskid) {//done
            $scope.pageinfo = "Edit Task";
            $scope.var_task = "Update Task";
            $scope.isTable = false;
            $scope.IsVisible3 = false;
            $scope.IsVisible2 = true;
            $scope.IsVisible1 = false;
            $scope.IsVisible = $scope.IsVisible ? false : true;
            var user_data = 'task_id=' + taskid;
            //alert(user_data);
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/gettask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            //  $route.reload();
            $scope.task = r['task'];
            //alert($scope.task[0].Assign_belongs_to);
//            $scope.Task = $scope.task.task_description; priority
//                    $scope.priority = $scope.task.priority;
//            $scope.assigndept = $scope.task.task_assign_dept;
            //$scope.data1 = r['check1'];
            });
            };
            $scope.completetask = function (task, user) {

            var user_data = 'task_id=' + task + '&u_id=' + user;
            //alert(user_data);
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/completetask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            });
            //window.location.href = 'http://localhost/gatha_web_surya/Welcome/login';  
            };
            //Current Task by default
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/getActiveTasks',
                    //data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            if (r.success == true){
            $scope.data = r['task_data'];
            } else {
            $scope.data = "No data found"
            }
            //$scope.data1 = r['check1'];
            }).error(function () {
            $scope.data = "error in fetching data";
            });
            //Current Task CLick
            $scope.getCurrentTask = function () {//done
            $scope.pageinfo = "current task";
            $scope.comment_div = false;
            $scope.mytaskbtn = false;
            $scope.acptdate = false;
            $scope.todept = true;
            $scope.isTable = true;
            $scope.isCompleted = false;
            $scope.IsCurrent = true;
            $scope.IsCurrent1 = true;
            $scope.IsVisible = false;
            $scope.mytaskhead = true;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/getActiveTasks',
                    //data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            $scope.data = r['task_data'];
            //$scope.data1 = r['check1'];
            }).error(function () {
            $scope.data = "error in fetching data";
            });
            };
            //Completed Task
            $scope.getCompletedTask = function (userid) {
            $scope.pageinfo = "MY completed task";
            $scope.comment_div = false;
            $scope.acptdate = true;
            $scope.mytaskbtn = false;
            $scope.mytaskhead = false;
            $scope.isTable = true;
            $scope.isCompleted = true;
            $scope.IsCurrent = false;
            $scope.IsCurrent1 = false;
            $scope.IsVisible = false;
            $scope.todept = false;
            var user_data = 'user_id=' + userid;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/completedTaskData',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            $scope.data = r['task_data'];
            }).error(function () {
            $scope.data = "error in fetching data";
            });
            }
            }]);

        </script>
    </body>
</html>