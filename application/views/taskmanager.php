<html ng-app="fetch">
    <head>
        <title>Task Manager with PHP</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/1.3.1/ui-bootstrap-tpls.min.js"></script>
    </head>
    <?php
    $user_id = $rows[0]['user_id'];

//    echo '<pre>';
//    print_r($rows);
    ?>
    <body style="background-color: #f0f4c3">
        <div class ="" style="text-align: center;margin: auto;background-color: #e0f2f1;padding:20px; width: 80%;" ng-controller="dbCtrl" ng-mousedown="errorMsg = false;">
            <div class="well" style="height:70px">
                <div style="float:left;"><span style="font-size: 25px;color:#7986cb;">Task Info:{{pageinfo}}</span></div>
                <div style="float:right;margin-bottom: 7px;"><input type="text" ng-model="searchFilter" class="form-control" placeholder="search"></div>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="logout()"  >log out</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="createtask_div()"  >Create Task</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="getCurrentTask()">Current Task</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="getCompletedTask(<?php echo $user_id ?>)">Completed Task</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="getmyTask(<?php echo $user_id ?>)">My Task</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="Assign_div()">Assign to User</button>
                <button type="button" class="btn btn-default" style="float:right;margin-bottom: 7px;margin-right: 5px" ng-click="Task_assign_status()">Task_assign_status</button>
            </div>
            <div ng-show = "pageinfo == 'Edit Task' || pageinfo == 'create task'" style="position: relative;background-color:#efebe9;padding: 30px;z-index: 99999; margin: auto; text-align: center;" class="container">
                <h3>{{var_task}}</h3>
                <form name="login"  class="form-horizontal" method="POST">
                    <div style="margin-bottom: 25px;margin-left: 10%" class="input-group">
                        <span style="margin-left:25px" class="input-group-addon ">Assign to</span>
                        <select style="width:80%" class="form-control"ng-model="assigndept" ng-options="item for item in dept" required>
                            <option value="">Select Department</option> 
                        </select>
                        <span  class="input-group-addon">priority</span>
                        <select style="width:80%" class="form-control"ng-model="priority" ng-options="item for item in prior" required>
                            <option value="">Select Priority</option>
                        </select>
                    </div>
                    <div style="margin-bottom: 25px;margin-left: 10%" class="input-group">
                        <textarea rows="10" cols="145" placeholder="please enter your Task here" ng-model="Task"></textarea>
                    </div>
                    <div class="form-group" style="margin-bottom: 25px; margin-left: 9%;">
                        <div class="col-sm-5 controls" ng-show = "pageinfo == 'create task'">
                            <button class="btn btn-primary pull-right" ng-click="createtask(<?php echo $user_id ?>)"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Submit</button>
                        </div>
                        <div class="col-sm-5 controls" ng-show = "pageinfo == 'Edit Task'">
                            <button  class="btn btn-primary pull-right" ng-click="Updatetask(task.task_id)"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Update</button>
                        </div>
                        <div class="col-sm-5 controls" >
                            <button  class="btn btn-primary pull-left" ng-click="cancel()"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <div ng-show="pageinfo == 'Assign_task_user'" style="position: relative;background-color:#efebe9;padding: 30px;z-index: 99999; margin: auto; text-align: center;" class="container">
                <div style="margin-bottom: 25px;margin-left: 10%" class="input-group">
                    <span style="margin-left:25px" class="input-group-addon ">Department</span>
                    <select style="width:80%" class="form-control"ng-model="assigndept" ng-options="item for item in dept" required ng-change="get_user_info(assigndept)">
                        <option value="">Select Department</option> 
                    </select>
                    </span>
                    <span style="margin-left:25px" class="input-group-addon ">Assign to</span>
                    <select style="width:80%" class="form-control"ng-model="assigneduser"  required>
                        <option value="" >Select Name</option> 
                        <option  ng-repeat="users in userinfo" value="{{users.user_id}}">{{users.u_firstname}} {{users.u_lastname}}</option> 
                    </select>
                    </span>
                    <span  class="input-group-addon">priority</span>
                    <select style="width:80%" class="form-control"ng-model="priority" ng-options="item for item in prior" required>
                        <option value="">Select Priority</option>
                    </select> 
                    </span>
                </div>
                <div style="margin-bottom: 25px;margin-left: 10%" class="input-group">
                    <textarea rows="10" cols="145" placeholder="write your task here" ng-model="assign_task" required autofocus></textarea>
                </div>
                <div class="col-sm-5 controls" >
                    <button  class="btn btn-primary pull-right" ng-click="assign_task_to_selected_user(<?php echo $user_id ?>)" ng-show="assign_task"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Submit</button>
                </div>
                <div class="col-sm-5 controls" >
                    <button  class="btn btn-primary pull-left" ng-click="cancel()"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Back</button>
                </div>

            </div>
            <div ng-show="pageinfo == 'comment'" style="position: relative;background-color:#efebe9;padding: 30px;z-index: 99999; margin: auto; text-align: center;" class="container">

                <div style="margin-bottom: 25px;margin-left: 10%" class="input-group">
                    <textarea rows="10" cols="145" placeholder="To cancel the task ,please enter why you want to cancel the task" ng-model="comment" required autofocus></textarea>
                </div>
                <div class="col-sm-5 controls" >
                    <button  class="btn btn-primary pull-right" ng-click="canceltask(<?php echo $user_id ?>)" ng-show="comment"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Cancel</button>
                </div>
                <div class="col-sm-5 controls" >
                    <button  class="btn btn-primary pull-left" ng-click="cancel()"><i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Back</button>
                </div>

            </div>
            <!--ng-show="pageinfo == 'notification'"-->
            <div ng-show="pageinfo == 'notification'" ng-repeat="task in tasknotification" style="background-color: #ffcdd2;padding: 1%;">
                <div  style="border: 2px solid black;min-height: 380px;" >
                    <h4 class="well" style="color:#827717;border: 2px solid black;width:50%;margin:1% 25%;">  {{message}} </h4>
                    <!--<div style="width: 100%;min-height:200px">-->

                    <div class="well" style="width:20%;float:left;margin:0% 2%;border: 2px solid black;" >
                        <h3> Assigned by:</h3>{{task.u_email}} &nbsp;&nbsp;&nbsp;&nbsp; <h3> priority:</h3>{{task.priority}} <br/>
                        <button style="margin-top: 5px;border-radius: 15px;width: 130px; height: 30px; border: 1px solid black;background-color: green;color: white;" type="" ng-click="notification_action(task.task_id,<?php echo $user_id; ?>, action = 1)">Accept</button>
                        <br/>  <button   style="border-radius: 15px;margin-top: 5px;width: 130px; height: 30px; border: 1px solid black;background-color: red;color: white;" type="" ng-click="notification_action(task.task_id,<?php echo $user_id; ?>, action = 2)">cancel</button>
                    </div>

                    <div class="well" style="border:2px solid black;width:70%;margin:2% 25%"><b style="font-family: Zapf-Chancery;font-size: 30px">TASK</b><br />{{task.task_description}}</div>

                    <!--</div>-->
                </div>
            </div>
            <table class="table table-hover"   ng-show="pageinfo != 'comment' && pageinfo != 'Edit Task' && pageinfo != 'create task' && pageinfo != 'Assign_task_user' && pageinfo != 'notification'">
                <thead>

                <th ng-repeat="th in header">{{th}}</th>

                </thead>
                <tbody>

                    <tr ng-repeat="users in data| filter:searchFilter">
                        <td ng-repeat="th in key">{{users[th]}}</td>
                        <td>
                            <span ng-show="pageinfo == 'Task_assign_status' && users.is_accepted == 0">Yet not accepted by user </span>
                            <span ng-show="pageinfo == 'Task_assign_status' && users.is_accepted == 1">Task Accepted </span>
                            <span ng-show="pageinfo == 'Task_assign_status' && users.is_accepted == 2">Task canceled by user</span>
                            <span ng-show="pageinfo == 'current task' && users.is_accepted == 1">Already Accepted</span>
                            &nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp<button  ng-show= "pageinfo == 'Task_assign_status' && users.is_accepted == 2" style="border-radius: 15px;width: 130px; height: 30px; border: 1px solid black;background-color: green;color: white;" type=""  ng-click="Edit_assign_task(users.task_description)">Edit</button>
                            <button  ng-show= "pageinfo == 'MY task'" style="border-radius: 15px;width: 130px; height: 30px; border: 1px solid black;background-color: green;color: white;" type=""  ng-click="completetask(users.task_id, '<?php echo $user_id ?>')">complete</button>
                            <button   style="border-radius: 15px;width: 130px; height: 30px; border: 1px solid black;background-color: green;color: white;" type=""  ng-show="users.is_accepted == 0"	 ng-click="taskaccept(users.task_id, '<?php echo $user_id ?>')">Task Accept</button>
                            <button style="border-radius: 15px;width: 100px; height: 30px; border: 1px solid black;background-color: #25A0E6;color: white;" type="" ng-show="users.created_by_user_id == '<?php echo $user_id; ?>' && users.is_accepted == '0'" ng-click="getEdittask(users.task_id)">Edit Task</button>
                            <button   ng-show= "pageinfo == 'MY task'" style="border-radius: 15px;width: 130px; height: 30px; border: 1px solid black;background-color: red;color: white;" type="" ng-click="taskcancel_div(users.task_id)">cancel</button>
                        </td>
                    </tr>
                </tbody>

            </table>
            <div class="alert" ng-show="errorMsg" ng-style="myObj">
                <span class="glyphicon glyphicon-hand-right"></span>&nbsp;&nbsp;{{errorMsg}}
            </div>

        </div>
        <script type="text/javascript">

            var fetch = angular.module('fetch', []);
            fetch.controller('dbCtrl', ['$scope', '$http', function ($scope, $http) {


            $scope.pageinfo = "current task";
            $scope.dept = ["ANDROID", "IOS", "PHP"];
            $scope.prior = ["HIGH", "MEDIUM", "LOW"];
            $scope.header = ["Task_id", "Asignee_email", "Assignee_belongs_to_dept", "Task_assign_to_dept", "priority", "Task", "Created_date"];
            $scope.key = ["task_id", "u_email", "u_dept_type", "task_assign_dept", "priority", "task_description", "created_date"];
            
            //edit task assign
            $scope.Edit_assign_task = function(task){
              $scope.pageinfo= "Assign_task_user";
              $scope.assign_task = task;
            }
            
            //task assign status
            $scope.Task_assign_status = function () {
            $scope.header = ["Task_id", "Task_assign_to", "Task", "priority", "Status"];
            $scope.key = ["task_id", "u_email", "task_description", "priority"];
            $scope.pageinfo = "Task_assign_status";
            var user_data = "user_id=" +<?php echo $user_id; ?>;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/Task_assign_status',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            if (r.success == true){
            $scope.data = r['task'];
            } else {
            $scope.data = r.message;
            }
            })

            }

            //logout
            $scope.logout = function () {
            window.location.href = 'http://localhost/angular_js/web/Welcome/logout';
            }

            //notification action i.e accept and cancel
            $scope.notification_action = function (taskid, userid, action) {
            var user_data = 'user_id=' + userid + '&task_id=' + taskid + '&action=' + action;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/notification_action',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
            if (data.success == true) {
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            } else {
            $scope.errorMsg = "Invalid ";
            }
            });
            };
            //send task notification to the user
            $scope.notification_asgn_user_task = function () {
            var user_data = 'user_id=' + <?php echo $user_id ?>;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/notification_asgn_user_task',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                    .success(function (r) {
                    if (r.success == true){
                    $scope.pageinfo = "notification";
                    $scope.tasknotification = r['task'];
                    $scope.message = r.message;
                    }
                    })

            }

            //check for notification in every 30 sec.
            setInterval(function(){

            $scope.notification_asgn_user_task();
            //$scope.createtask_div();
            }, 30000)//30 sec

                    //assign task to a specific user

                    $scope.assign_task_to_selected_user = function (userid) {
                        
                    //   var user_data = 'department=' + $scope.assigndept + '&assign_user_id=' + $scope.assigneduser + '&assignee_user_id=' + user + '&task_desc=' + $scope.assign_task + '&task_priority=' + $scope.priority;
                    // alert();
                    var user_data = 'department=' + $scope.assigndept + '&assign_user_id=' + $scope.assigneduser + '&task_desc=' + $scope.assign_task + '&task_priority=' + $scope.priority + '&assignee_user_id=' + userid;
                    $http({
                    method: 'POST',
                            url: 'http://localhost/angular_js/api/Welcome/assign_task_to_user',
                            data: user_data,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (r) {
                    if (r.success == true){
                    window.location.href = 'http://localhost/angular_js/web/Welcome/index';
                    }
                    })

                    };
            //get_user_according to department.
            $scope.get_user_info = function (dept) {
            var user_data = 'department=' + dept;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/get_user_info_according_dept',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            if (r.success == true){
            $scope.userinfo = r['user_data_according_dept'];
            }
            })


            }
            // ASSign task div for a particular user
            $scope.Assign_div = function () {
            $scope.old_pageinfo = $scope.pageinfo;
            $scope.assign_task = "";
            $scope.pageinfo = "Assign_task_user";
            }

            //canceltask
            $scope.canceltask = function (userid) {
            var user_data = 'task_id=' + $scope.taskcancel + '&comment=' + $scope.comment + '&user_id=' + userid;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/canceltask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            if (r.success == true){
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            } else {
            $scope.data = r.message;
            }
            })

            };
            //cancel button function.....to move back
            $scope.cancel = function () {
            $scope.pageinfo = $scope.old_pageinfo;
            };
            //task cancel div
            $scope.taskcancel_div = function (taskid) {
            $scope.taskcancel = taskid;
            $scope.old_pageinfo = $scope.pageinfo;
            $scope.pageinfo = "comment";
            };
            //getmytask function
            $scope.getmyTask = function (user) {//done
            $scope.data = {};
            $scope.pageinfo = "MY task";
            $scope.header = ["Task_id", "Task_assign_to_dept", "priority", "Task", "Created_date", "Accepted date"];
            $scope.key = ["task_id", "task_assign_dept", "priority", "task_description", "created_date", "accepted_date"];
            var user_data = 'user_id=' + user;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/getAcceptedTasks',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            if (r.success == true){
            $scope.data = r['accept_task_data'];
            } else {
            $scope.data = r.message;
            }
            }).error(function () {
            $scope.data = "error in fetching data";
            });
            };
            //task accept
            $scope.taskaccept = function(task_id, user_id){//done
            var user_data = 'task_id=' + task_id + '&user_id=' + user_id;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/taskaccept',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
            if (data.success == true) {
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            } else {
            $scope.errorMsg = "Invalid ";
            }
            });
            }

            //update task
            $scope.Updatetask = function (user) {//done

            var user_data = '&assigndept=' + $scope.assigndept + '&priority=' + $scope.priority + '&task_description=' + $scope.Task + '&task_id=' + user;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/Updatetask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
            if (data.success == true) {
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            } else {
            $scope.errorMsg = "Invalid ";
            }
            });
            }


            //create task
            $scope.createtask = function (user) {//done'

            var user_data = 'assigndept=' + $scope.assigndept + '&priority=' + $scope.priority + '&task_desc=' + $scope.Task + '&user_id=' + user;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/createtask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {

            if (data.success == true) {
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            } else {
            $scope.errorMsg = "Invalid Email and Password";
            }
            });
            }

            //create task div
            $scope.createtask_div = function () {//done
            $scope.old_pageinfo = $scope.pageinfo;
            $scope.pageinfo = "create task";
            $scope.var_task = "Create Task";
            $scope.asigneedept = "";
            $scope.assigndept = "";
            $scope.priority = "";
            $scope.Task = "";
            };
            //edit task
            $scope.getEdittask = function (taskid) {//done
            $scope.old_pageinfo = $scope.pageinfo;
            $scope.pageinfo = "Edit Task";
            $scope.var_task = "Update Task";
            var user_data = 'task_id=' + taskid;
            //alert(user_data);
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/gettask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            //  $route.reload();
            $scope.task = r['task'];
            $scope.Task = $scope.task.task_description;
            });
            };
            //completetask
            $scope.completetask = function (task, user) {

            var user_data = 'task_id=' + task + '&u_id=' + user;
            //alert(user_data);
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/completetask',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            window.location.href = 'http://localhost/angular_js/web/Welcome/index';
            });
            //window.location.href = 'http://localhost/gatha_web_surya/Welcome/login';  
            };
            //Current Task by default
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/getActiveTasks',
                    //data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            if (r.success == true){
            $scope.data = r['task_data'];
            } else {
            $scope.data = "No data found"
            }
            //$scope.data1 = r['check1'];
            }).error(function () {
            $scope.data = "error in fetching data";
            });
            //Current Task CLick
            $scope.getCurrentTask = function () {//done
            $scope.pageinfo = "current task";
            $scope.header = ["Task_id", "Asignee_email", "Assignee_belongs_to_dept", "Task_assign_to_dept", "priority", "Task", "Created_date"];
            $scope.key = ["task_id", "u_email", "u_dept_type", "task_assign_dept", "priority", "task_description", "created_date"];
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/getActiveTasks',
                    //data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            $scope.data = r['task_data'];
            //$scope.data1 = r['check1'];
            }).error(function () {
            $scope.data = "error in fetching data";
            });
            };
            //Completed Task
            $scope.getCompletedTask = function (userid) {
            $scope.pageinfo = "MY completed task";
            $scope.header = ["Task_id", "Task", "priority", "Created_date", "Accepted date", "Completed_date"];
            $scope.key = ["task_id", "task_description", "priority", "created_date", "accepted_date", "completed_date"];
            var user_data = 'user_id=' + userid;
            $http({
            method: 'POST',
                    url: 'http://localhost/angular_js/api/Welcome/completedTaskData',
                    data: user_data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (r) {
            $scope.data = r['task_data'];
            }).error(function () {
            $scope.data = "error in fetching data";
            });
            }
            }]);

        </script>
    </body>
</html>